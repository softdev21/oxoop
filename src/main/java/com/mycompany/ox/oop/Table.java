/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ox.oop;

/**
 *
 * @author commis
 */
public class Table {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    private Player playerX;
    private Player playerO;
    private Player currenPlayer;
    private int lastrow;
    private int lastcol;
    private Player winner;
    int turn;
    private boolean Finish = false;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currenPlayer = X;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currenPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getCurrentplayer() {
        return currenPlayer;
    }

    public void switchPlayer() {
        if (currenPlayer == playerX) {
            currenPlayer = playerO;
        } else {
            currenPlayer = playerX;
        }
    }

    void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currenPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currenPlayer;
        if (currenPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currenPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currenPlayer;
        if (currenPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkCrossX1() {
        if (table[0][0] == 'X') {
            if (table[1][1] == 'X') {
                if (table[2][2] == 'X') {
                    Finish = true;
                    winner = currenPlayer;
                    if (currenPlayer == playerO) {
                        playerO.win();
                        playerX.lose();
                    } else {
                        playerO.lose();
                        playerX.win();
                    }
                }
            }
        }
    }

    void checkCrossX2() {
        if (table[0][2] == 'X') {
            if (table[1][1] == 'X') {
                if (table[2][0] == 'X') {
                    Finish = true;
                    winner = currenPlayer;
                    if (currenPlayer == playerO) {
                        playerO.win();
                        playerX.lose();
                    } else {
                        playerO.lose();
                        playerX.win();
                    }
                }
            }
        }
    }

    void checkCrossO1() {
        if (table[0][0] == 'O') {
            if (table[1][1] == 'O') {
                if (table[2][2] == 'O') {
                    Finish = true;
                    winner = currenPlayer;
                    if (currenPlayer == playerO) {
                        playerO.win();
                        playerX.lose();
                    } else {
                        playerO.lose();
                        playerX.win();
                    }
                }
            }
        }
    }

    void checkCrossO2() {
        if (table[0][2] == 'O') {
            if (table[1][1] == 'O') {
                if (table[2][0] == 'O') {
                    Finish = true;
                    winner = currenPlayer;
                    if (currenPlayer == playerO) {
                        playerO.win();
                        playerX.lose();
                    } else {
                        playerO.lose();
                        playerX.win();
                    }
                }
            }
        }
    }

    void checkDrew() {
        if (turn == 9) {
            winner = null;
            Finish = true;
            playerO.getDraw();
            playerX.getDraw();
        }
    }

    public void CheckWin() {
        checkRow();
        checkcol();
        checkCrossX1();
        checkCrossX2();
        checkCrossO1();
        checkCrossO2();
    }

    public boolean isFinish() {
        return Finish;
    }

    public Player getWinner() {
        return winner;
    }
}
