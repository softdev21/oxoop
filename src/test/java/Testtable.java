/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.ox.oop.Player;
import com.mycompany.ox.oop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author commis
 */
public class Testtable {

    public Testtable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    ///TestX
    @Test
    public void testRowX1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testRowX2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testRowX3() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testColX1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testColX2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testColX3() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testdiagonalX1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    @Test
    public void testdiagonalX2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 2);
        table.setRowCol(1, 1);
        table.setRowCol(0, 0);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }

    ///TestO
    @Test
    public void testRowO1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testRowO2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testRowO3() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testColO1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testColO2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testColO3() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testdiagonalO1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }

    @Test
    public void testdiagonalO2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(2, 2);
        table.setRowCol(1, 1);
        table.setRowCol(0, 0);
        table.CheckWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
}
